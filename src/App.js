import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import NavBar from './NavBar/NavBar';
import Movies from './Movies/Movies';
import Movie from './Movies/Movie/Movie';
import WatchMovie from './Movies/WatchMovie/WatchMovie';

class App extends Component {
  render() {
    return (
      <div className="App">
        <NavBar></NavBar>
      
 
        <Route exact path='/' component={Movies}/>
        <Route exact path='/movies' component={Movies}/>
        <Route exact path='/movie/:movieId' component={Movie}/>
        <Route exact path='/watch-movie/:movieId' component={WatchMovie}/>
      </div>
    );
  }
}

export default App;